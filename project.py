from flask import Flask, request, jsonify, make_response
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import and_
from flask_marshmallow import Marshmallow
from flask_bcrypt import Bcrypt
import uuid
import jwt
import datetime
from functools import wraps
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from slugify import slugify
import os, sys, shutil, json
import os.path
from flask_cors import CORS, cross_origin
import transcode
import subprocess
from flask_inputs import Inputs
import glob



# from flask_cors import CORS, cross_origin
# init app
app = Flask(__name__)
# CORS(app, support_credentials=True)

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:mypass@localhost/sampledb'

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = '#th3coder$$'
app.config['PATH_NAME'] =  os.path.dirname(sys.argv[0])
app.config['VIDEOS_PATH_NAME'] =  "assets/media/videos"
app.config['TRAILER_PATH_NAME'] =  "assets/media/trailers"
app.config['VIDEOS_IMAGES_PATH_NAME'] =  "assets/media/videos_images"
app.config['SERIES_IMAGES_PATH_NAME'] =  "assets/media/series_images"
app.config["ALLOWED_IMAGE_EXTENSIONS"] = ["JPEG", "JPG", "PNG"]
app.config["ALLOWED_VIDEO_EXTENSIONS"] = ["MP4"]

# init db
db = SQLAlchemy(app)
# init Ma
ma = Marshmallow(app)
# init Bycrypt
bcrypt = Bcrypt(app)


# association table

video_genre_association = db.Table('video_genre_association', db.metadata, db.Column('id', db.Integer, primary_key=True), db.Column('video_id', db.Integer, db.ForeignKey('videos.id')), db.Column('genre_id', db.Integer, db.ForeignKey('video_genres.id')))

videos_tags_association = db.Table('videos_tags_association', db.metadata, db.Column('id', db.Integer, primary_key=True), db.Column('video_id', db.Integer, db.ForeignKey('videos.id')), db.Column('tag_id', db.Integer, db.ForeignKey('videos_tags.id')))
audios_tags_association = db.Table('audios_tags_association', db.metadata, db.Column('id', db.Integer, primary_key=True), db.Column('audio_id', db.Integer, db.ForeignKey('audios.id')), db.Column('tag_id', db.Integer, db.ForeignKey('audios_tags.id')))
video_trailer_association = db.Table('video_trailer_association', db.metadata, db.Column('id', db.Integer, primary_key=True), db.Column('video_id', db.Integer, db.ForeignKey('videos.id'), unique=True), db.Column('trailer_id', db.Integer, db.ForeignKey('trailers.id'), unique=True))

def create_error_log(error):  # pass error as string
    try:
        f = open("error_log.log", "a+")
        f.write("Log: " + str(datetime.datetime.now())+ " " + str(error)  + " \r\n")
        f.close()
    except Exception as e:
        pass

def user_token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None
        if 'x-access-token' in request.headers:
            token = request.headers['x-access-token']
        if not token:
            return jsonify({'auth_error': {'message': 'Kindly login'}})
        try:
            data = jwt.decode(token, app.config['SECRET_KEY'])
            current_user = User.query.filter_by(public_id=data['public_id']).first()
            if not current_user:
                return jsonify({'auth_error': {'message': 'User not found'}})
        except Exception as e:
            return jsonify({'auth_error': {'message': 'Token mismatch '}})

        return f(current_user, *args, **kwargs)

    return decorated

def admin_token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None

        if 'x-access-token' in request.headers:
            token = request.headers['x-access-token']
        if not token:
            return jsonify({'auth_error': {'message': 'Kindly login'}})
        try:
            data = jwt.decode(token, app.config['SECRET_KEY'])
            current_admin = User.query.filter_by(public_id=data['public_id']).filter_by(type='admin').first()
            if not current_admin:
                return jsonify({'auth_error': {'message': 'User not found'}})
        except Exception as e:
            create_error_log(str(e))
            return jsonify({'auth_error': {'message': 'Token error'}})
        return f(current_admin, *args, **kwargs)

    return decorated

def allowed_image(filename):
    # We only want files with a . in the filename
    if not "." in filename:
        return jsonify({'input_error': {'message': 'Upload correct images'}})

    # Split the extension from the filename
    ext = filename.rsplit(".", 1)[1]

    # Check if the extension is in ALLOWED_IMAGE_EXTENSIONS
    if not ext.upper() in app.config["ALLOWED_IMAGE_EXTENSIONS"]:
        return False
    else:
        return True

def allowed_video(filename):
    # We only want files with a . in the filename
    if not "." in filename:
        return jsonify({'input_error': {'message': 'Only mp4 videos are allowed'}})

    # Split the extension from the filename
    ext = filename.rsplit(".", 1)[1]
    # Check if the extension is in ALLOWED_IMAGE_EXTENSIONS
    if not ext.upper() in app.config["ALLOWED_VIDEO_EXTENSIONS"]:
        return False
    else:
        return True

def get_file_extension(filename):
    if not "." in filename:
        return jsonify({'input_error': {'message': 'Upload correct files'}})

    # Split the extension from the filename
    ext = filename.rsplit(".", 1)[1]
    return ext.upper()


def get_actual_filename(name):
    name = "%s[%s]" % (name[:-1], name[-1])
    return glob.glob(name)[0]

def getLength(input_video):
    result = subprocess.Popen('ffprobe -i input_video -show_entries format=duration -v quiet -of csv="p=0"', stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
    output = result.communicate()
    return output[0]

def get_duration(file):
    duration = subprocess.check_output(['ffprobe', '-i', file, '-show_entries', 'format=duration', '-v', 'quiet', '-of', 'csv=%s' % ("p=0")])
    print(duration)

def delete_files(files_to_delete):
    try:
        for file_to_delete in files_to_delete:
            if os.path.isfile(file_to_delete):
                os.remove(file_to_delete)
            elif os.path.isdir(file_to_delete):
                shutil.rmtree(file_to_delete)
            else:
                create_error_log("Can't find the folder "+str(file_to_delete))
    except Exception as e:
        create_error_log(str(e))
        pass

def delete_objects(objects_to_delete):
    try:
        for object_to_delete in objects_to_delete:
            db.session.delete(object_to_delete)
    except Exception as e:
        create_error_log(str(e))
        pass

class Video(db.Model):
    __tablename__ = "videos"
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String(100), unique=True, nullable=False)
    title = db.Column(db.String(255), nullable=False)
    slug = db.Column(db.String(255), unique=True)
    status = db.Column(db.String(20),  default='review', nullable=False)
    artist_id = db.Column(db.Integer, db.ForeignKey('artists.id'))
    duration = db.Column(db.String(10), nullable=True)
    type = db.Column(db.String(20), db.CheckConstraint(type in(['single', 'series'])), nullable=False)  #single, series
    series_id = db.Column(db.Integer, db.ForeignKey('series.id'), nullable=True, default=None)
    trailer_id = db.Column(db.Integer, db.ForeignKey('trailers.id'), nullable=True, default=None)
    video_file = db.Column(db.String(100), nullable=False)
    video_file_type = db.Column(db.String(7), nullable=False)
    large_image = db.Column(db.String(100), default=None, unique=True, nullable=True)
    portrait_image = db.Column(db.String(100), unique=True, nullable=False)
    landscape_image = db.Column(db.String(100), unique=True, nullable=False)
    square_image = db.Column(db.String(100), unique=True, nullable=False)
    description = db.Column(db.Text, nullable=True)
    created_by = db.Column(db.String(100), nullable=False)
    verified_by = db.Column(db.String(100), nullable=True)
    created_on = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow, nullable=False)
    artist = db.relationship("Artist", uselist=False, foreign_keys=[artist_id], back_populates="videos")
    likes = db.relationship("VideoLike", uselist=False, back_populates="video")
    views = db.relationship("VideoView", uselist=False, back_populates="video")
    genres = db.relationship("VideoGenre", secondary=video_genre_association, back_populates="videos")
    tags = db.relationship("VideosTag", secondary=videos_tags_association, back_populates="videos")
    series = db.relationship("Series", foreign_keys=[series_id], back_populates="videos")
    playlist_videos = db.relationship("VideoPlaylist", uselist=False, back_populates="videos")

    def __init__(self, public_id, title, slug, status,artist_id, duration, video_type, series_id, video_file, video_file_type, large_image, portrait_image, landscape_image, square_image, description, created_by, verified_by):
        self.public_id = public_id
        self.title = title
        self.slug = slug
        self.status = status
        self.artist_id = artist_id
        self.duration = duration
        self.series_id = series_id
        self.video_file = video_file
        self.video_file_type = video_file_type
        self.type = video_type
        self.large_image = large_image
        self.portrait_image = portrait_image
        self.landscape_image = landscape_image
        self.square_image = square_image
        self.description = description
        self.created_by = created_by
        self.verified_by = verified_by

class User(db.Model):
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String(50), unique=True, nullable=False)
    first_name = db.Column(db.String(60))
    last_name = db.Column(db.String(60))
    email = db.Column(db.String(60), unique=True, nullable=False)
    phone = db.Column(db.String(60), unique=True, nullable=True)
    password = db.Column(db.String(200), nullable=False)
    status = db.Column(db.String(15), default='active', nullable=False)
    profile_photo = db.Column(db.String(255), nullable=True)
    created_on = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow, nullable=False)
    type = db.Column(db.String(15), nullable=False) #user, admin, artist
    video_likes = db.relationship("VideoLike", uselist=False, back_populates="user")
    video_views = db.relationship("VideoView", uselist=False, back_populates="user")
    audio_likes = db.relationship("AudioLike", uselist=False, back_populates="user")
    played_audios = db.relationship("PlayedAudio", uselist=False, back_populates="user")
    follows = db.relationship("Follow", uselist=False, back_populates="user")
    feedbacks = db.relationship("Feedback", uselist=False, back_populates="user")
    artist = db.relationship("Artist", uselist=False, back_populates="user")     #check here for errors. artist ad foreign key not found
    playlist_audios = db.relationship("AudioPlaylist", uselist=False, back_populates="user")
    playlist_videos = db.relationship("VideoPlaylist", uselist=False, back_populates="user")

    def __init__(self, public_id, first_name, last_name, type, email, password):
        self.public_id = public_id
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.password = password
        self.type = type


class Trailer(db.Model):
    __tablename__ = "trailers"
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String(100), unique=True)
    video_id = db.Column(db.Integer, db.ForeignKey('videos.id'), unique=True, nullable=False)
    title = db.Column(db.String(150), nullable=False)
    slug = db.Column(db.String(200), unique=True, nullable=False)
    status = db.Column(db.String(20), nullable=False)
    trailer_file = db.Column(db.String(200), nullable=False)
    trailer_file_type = db.Column(db.String(5), nullable=False)
    created_on = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow, nullable=False)
    #video = db.relationship("Video", foreign_keys=[video_id],remote_side='video_trailer_association.video_id', back_populates="trailer")
 
    def __init__(self, public_id, video_id, title, slug, status, trailer_file, trailer_file_type):
        self.public_id = public_id
        self.video_id = video_id
        self.title = title
        self.slug = slug
        self.status = status
        self.trailer_file = trailer_file
        self.trailer_file_type = trailer_file_type

class VideoGenre(db.Model):
    __tablename__ = "video_genres"
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), nullable=False)
    slug = db.Column(db.String(100), unique=True, nullable=False)
    videos = db.relationship("Video", secondary=video_genre_association, back_populates="genres")

    def __init__(self, title, slug):
        self.title = title
        self.slug = slug

class VideosTag(db.Model):
    __tablename__ = "videos_tags"
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(30), nullable=False)
    slug = db.Column(db.String(40), unique=True, nullable=False)
    videos = db.relationship("Video", secondary=videos_tags_association, back_populates="tags")

    def __init__(self, title, slug):
        self.title = title
        self.slug = slug

# user routes begin here
@app.route('/user', methods=['POST'])
def add_user():
    try:
        first_name = request.form['first_name']
        last_name = request.form['last_name']
        email = request.form['email']
        password = request.form['password']
        public_id = str(uuid.uuid4())
        type = 'user'
        password = bcrypt.generate_password_hash(password).decode('utf-8')
    except KeyError as keyError:
        return jsonify({'input_error': {'message': 'There is a missing key'}})
    except Exception as e:
        create_error_log(str(e))
        return jsonify({'db_error': {'message': 'There is an error. Check your header and request method'}})

    # check if email exists

    if not first_name:
        return jsonify({'input_error': {'message': 'First name is missing'}})
    if not last_name:
        return jsonify({'input_error': {'message': 'Last name is missing'}})
    if not email:
        return jsonify({'input_error': {'message': 'Email is missing'}})
    if not password:
        return jsonify({'input_error': {'message': 'Password is missing'}})

    # check if email exists
    similar_email = User.query.filter_by(email=email).first()
    if similar_email:
        return jsonify({'input_error': {'message': 'A user with similar email exists'}})

    new_user = User(public_id, first_name, last_name, type, email, password)
    try:
        db.session.add(new_user)
        db.session.commit()
        return jsonify({'response': {'message': 'Created successfully'}})
    except Exception as e:
        create_error_log(str(e))
        return jsonify({'db_error': {'message': 'There was a database error'}})

# video routes begin here
@app.route('/video', methods=['POST'])
@user_token_required
def add_video(current_user):
    try:
        uploaded_files = []
        created_objects = []
        this_user = user_schema.dump(current_user)
        # check if user has an artist account
        user_type = this_user['type']
        this_user_name = this_user['first_name']+" "+this_user['last_name']
        if user_type != 'admin' and user_type != 'artist':
            return jsonify({'auth_error': {'message': 'Operation not allowed'}})
        if user_type == 'artist':
            # get this artist
            current_artist = Artist.query.filter_by(user_id=this_user['id']).first()
            if not current_artist:
                return jsonify({'auth_error': {'message': 'Operation denied. Kindly login'}})
            this_artist = artist_schema.dump(current_artist)
            artist_id = this_artist['id']
        try:
            video_file = request.files['video_file']
            title = request.form['title']
            if user_type == 'admin':
                artist_id = request.form['artist_id']
            large_image = request.files['large_image']
            portrait_image = request.files['portrait_image']
            landscape_image = request.files['landscape_image']
            square_image = request.files['square_image']
            tags_arr = request.form['tags_arr']
            description = request.form['description']
            video_type = request.form['video_type']
            status = 'review'
        except Exception as e:
            create_error_log(str(e))
            return jsonify({'input_error': {'message': 'Some required fields are not filled.'}})

        # generate a unique id for the media
        public_id = str(uuid.uuid4())
        created_by = this_user['first_name']+" "+this_user['last_name']
        duration = 7.00

        if not artist_id:
            return jsonify({'input_error': {'message':'Artist is required'}})
        if user_type == 'admin':
            current_artist = Artist.query.filter_by(id=artist_id ).first()
            if not current_artist:
                return jsonify({'input_error': {'message': 'The artist cannot be found'}})

        if not large_image:
            return jsonify({'input_error': {'message' : 'Large Image is required'}})
        if not portrait_image:
            return jsonify({'input_error': {'message' : 'Portrait Image is required'}})
        if not landscape_image:
            return jsonify({'input_error': {'message' : 'Landscape Image is required'}})
        if not square_image:
            return jsonify({'input_error': {'message' : 'Square Image is required'}})
        if not video_file:
            return jsonify({'input_error': {'message': 'Video is missing'}})

        if large_image.filename == "":
            return jsonify({'input_error': {'message': 'Large image is missing'}})
        if portrait_image.filename == "":
            return jsonify({'input_error': {'message': 'Large image is missing'}})
        if landscape_image.filename == "":
            return jsonify({'input_error': {'message': 'Large image is missing'}})
        if square_image.filename == "":
            return jsonify({'input_error': {'message': 'Large image is missing'}})
        if video_file.filename == "":
            return jsonify({'input_error': {'message': 'Video file is missing'}})
    
        if not allowed_image(large_image.filename):
            return jsonify({'input_error': {'message': 'Kindly upload JPEG, JPG or PNG images only'}})
        if not allowed_image(portrait_image.filename):
            return jsonify({'input_error': {'message': 'Kindly upload JPEG, JPG or PNG images only'}})
        if not allowed_image(landscape_image.filename):
            return jsonify({'input_error': {'message': 'Kindly upload JPEG, JPG or PNG images only'}})
        if not allowed_image(square_image.filename):
            return jsonify({'input_error': {'message': 'Kindly upload JPEG, JPG or PNG images only'}})
        if not allowed_video(video_file.filename):
            return jsonify({'input_error': {'message': 'Only mp4 videos are allowed.'}})
        if video_type != 'series' and video_type != 'single':
            return jsonify({'input_error': {'message': 'Video Type should be either single or series.'}})
        if user_type == 'admin':
            if video_type == 'series':
                try:
                    series_id = request.form['series_id']
                except KeyError as e:
                    return jsonify({'input_error': {'message': 'Series is missing'}})
                except Exception as e:
                    create_error_log(str(e))
                    return jsonify({'db_error':{'message':'There was an intern error'}})
                if not series_id:
                    return jsonify({'input_error': {'message': 'Series is missing'}})
                # check if that series exists
                series = Series.query.filter_by(id=series_id).first()
                if not series:
                    return jsonify({'input_error': {'message':'The series selected does not exist'}})
            else:
                series_id =None
        else: 
            series_id =None
        # create videos folder
        try:
            if not os.path.exists(app.config['VIDEOS_PATH_NAME']):
                original_umask = os.umask(0o000)
                os.makedirs(app.config['VIDEOS_PATH_NAME'], 0o777)
        except FileExistsError as e:
            pass
        except Exception as e:
            create_error_log(str(e))
            return jsonify({'db_error': {'message': 'Folder creation error'}})
        finally:
            os.umask(0o000)
        
        # create videos images folder        
        try:
            if not os.path.exists(app.config['VIDEOS_IMAGES_PATH_NAME']):
                original_umask = os.umask(0o000)
                os.makedirs(app.config['VIDEOS_IMAGES_PATH_NAME'], 0o777)
        except FileExistsError as e:
            pass
        except Exception as e:
            create_error_log(str(e))
            return jsonify({'db_error': {'message': 'Folder creation error'}})
        finally:
            os.umask(0o000)

        # generate slug for media
        slug = slugify(title)
        video_slug = Video.query.filter_by(slug=slug).first()
        i = 1
        while video_slug:
            i = i + 1
            new_slug = slug + '_' + str(i)
            video_slug = Video.query.filter_by(slug=new_slug).first() 
            if not video_slug:
                slug = new_slug
                break      
        
        # upload large image
        ext = get_file_extension(large_image.filename)
        large_image_name = "large_" + public_id + "." + ext
        large_image.save(os.path.join(app.config['VIDEOS_IMAGES_PATH_NAME'], large_image_name))
        uploaded_files.append(app.config['VIDEOS_IMAGES_PATH_NAME']+"/"+large_image_name)

        # upload portrait image
        ext = get_file_extension(portrait_image.filename)
        portrait_image_name = "portrait_" + public_id + "." + ext
        portrait_image.save(os.path.join(app.config['VIDEOS_IMAGES_PATH_NAME'], portrait_image_name))
        uploaded_files.append(app.config['VIDEOS_IMAGES_PATH_NAME']+"/"+portrait_image_name)
            
        # upload landscape image
        ext = get_file_extension(landscape_image.filename)
        landscape_image_name = "landscape_" + public_id + "." + ext
        landscape_image.save(os.path.join(app.config['VIDEOS_IMAGES_PATH_NAME'], landscape_image_name))
        uploaded_files.append(app.config['VIDEOS_IMAGES_PATH_NAME']+"/"+landscape_image_name)
  
        # upload square image
        ext = get_file_extension(square_image.filename)
        square_image_name = "square_" + public_id + "." + ext
        square_image.save(os.path.join(app.config['VIDEOS_IMAGES_PATH_NAME'], square_image_name))
        uploaded_files.append(app.config['VIDEOS_IMAGES_PATH_NAME']+"/"+square_image_name)

        # upload video
        ext = get_file_extension(video_file.filename)
        video_file_name = "" + public_id + "." + ext
        video_file_type=ext
        video_file.save(os.path.join(app.config['VIDEOS_PATH_NAME'], video_file_name))

        #get media base name
        
        # if user is admin, convert the saved file to mpd
        if user_type == 'admin':
            status = 'active'
            try:
                subprocess.run([app.config['PATH_NAME'] + "/transcode.py", app.config['VIDEOS_PATH_NAME']+"/"+video_file_name])
                os.remove(app.config['VIDEOS_PATH_NAME']+"/"+video_file_name)
                video_folder_name = os.path.splitext(app.config['VIDEOS_PATH_NAME']+"/"+video_file_name)[0]
                
                uploaded_files.append(video_folder_name)
            

            except Exception as e:
                create_error_log(str(e))
                for uploaded_file in uploaded_files:
                    os.remove(uploaded_file)
                return jsonify({'db_error': {'message':'Video Conversion error'}})
        # save the video details.
        try:
            new_video = Video(public_id, title, slug, status,artist_id, duration, video_type, series_id, video_file_name, video_file_type, large_image_name, portrait_image_name, landscape_image_name, square_image_name, description, created_by, this_user_name)
            db.session.add(new_video)
            db.session.commit()
            created_objects.append(new_video)
        except Exception as e:
            create_error_log(str(e))
            delete_files(uploaded_files)
            delete_objects(created_objects)
            return jsonify({'db_error':{'message':'There was an internal error'}})

            # save the tag 
        if tags_arr:
            for tag_title in tags_arr:
                video_tag = VideosTag.query.filter_by(title=tag_title).first()
                if video_tag:
                    # get id and insert in into videos tag association table
                    tag_id = videos_tag_schema.dump(video_tag)['id']
                else:
                    slug = slugify(tag_title)
                    video_tag_slug = VideosTag.query.filter_by(slug=slug).first()
                    i = 1
                    while video_tag_slug:
                        i = i + 1
                        new_slug = slug + '_' + str(i)
                        video_tag_slug = VideosTag.query.filter_by(slug=new_slug).first() 
                        if not video_tag_slug:
                            slug = new_slug
                            break
                    new_video_tag = VideosTag(tag_title, slug)
                    db.session.add(new_video_tag)
                    db.session.commit()
                    created_objects.append(new_video_tag)
                    tag_id = new_video_tag.id
                # insert into videos_tags_association table
                db.engine.execute(videos_tags_association.insert(), tag_id=tag_id, video_id=new_video.id)
        return jsonify({'response': {'message': 'Video created successfully'}})
    except Exception as e:
        create_error_log(str(e))
        delete_files(uploaded_files)
        delete_objects(created_objects)
        return jsonify({'db_error': {'message': 'There was an internal error'}})
# end create video

# start edit video
# video routes begin here
@app.route('/videos/<public_id>', methods=['PUT'])
@admin_token_required
def update_video(current_admin, public_id):
    try:
        this_user = user_schema.dump(current_admin)
        # check if this video exists
        video = Video.query.filter_by(public_id=public_id).first()
        if not video:
            return jsonify({'not_found': {'message': 'This video cannot be found'}})
        this_video = video_schema.dump(video)
        if this_video['status'] == 'review':
            return jsonify({'error': {'message': 'Sorry. You cannot edit a video that is under review.'}})
        try:
            video_file = request.files['video_file']
            title = request.form['title']
            artist_id = request.form['artist_id']
            try:
                large_image = request.files['large_image']
            except KeyError as e:
                large_image = False
                pass
            try:
                portrait_image = request.files['portrait_image']
            except KeyError as e:
                portrait_image = False
                pass
        
            try:
                landscape_image = request.files['landscape_image']
            except KeyError as e:
                landscape_image = False
                pass
            try:
                square_image = request.files['square_image']
            except KeyError as e:
                square_image = False
                pass
            tags_arr = request.form['tags_arr']
            description = request.form['description']
            video_type= request.form['video_type']
            status = 'review'
        except KeyError as e:
            create_error_log(str(e))
            return jsonify({'input_error': {'message': 'Some required fields are not filled.'}})
        except Exception as e:
            create_error_log(str(e))
            return jsonify({'input_error': {'message': 'Some required fields are not filled.'}})

        # generate a unique id for the media
        duration = 7.00

        if not artist_id:
            return jsonify({'input_error': {'message':'Artist is required'}})

        artist = Artist.query.filter_by(id=artist_id).first()
        if not artist:
            return jsonify({'input_error': {'message':'The artist does not exist'}})
        if large_image:
            if large_image.filename == "":
                return jsonify({'input_error': {'message': 'Large image is missing'}})
            if not allowed_image(large_image.filename):
                return jsonify({'input_error': {'message': 'Kindly upload JPEG, JPG or PNG images only'}})        
        if portrait_image:
            if portrait_image.filename == "":
                return jsonify({'input_error': {'message': 'Large image is missing'}})
            if not allowed_image(portrait_image.filename):
                return jsonify({'input_error': {'message': 'Kindly upload JPEG, JPG or PNG images only'}})
        if landscape_image:
            if landscape_image.filename == "":
                return jsonify({'input_error': {'message': 'Large image is missing'}})
            if not allowed_image(landscape_image.filename):
                return jsonify({'input_error': {'message': 'Kindly upload JPEG, JPG or PNG images only'}})
        if square_image:
            if square_image.filename == "":
                return jsonify({'input_error': {'message': 'Large image is missing'}})
            if not allowed_image(square_image.filename):
                return jsonify({'input_error': {'message': 'Kindly upload JPEG, JPG or PNG images only'}})
        if video_file:
            if video_file.filename == "":
                return jsonify({'input_error': {'message': 'Video file is missing'}})
            if not allowed_video(video_file.filename):
                return jsonify({'input_error': {'message': 'Only mp4 videos are allowed.'}})

        if video_type != 'series' and video_type != 'single':
            return jsonify({'input_error': {'message': 'Video Type should be either single or series.'}})
     
        if video_type == 'series':
            try:
                series_id = request.form['series_id']
            except KeyError as e:
                return jsonify({'input_error': {'message': 'Series is missing'}})
            except Exception as e:
                create_error_log(str(e))
                return jsonify({'db_error':{'message':'There was an intern error'}})
            if not series_id:
                return jsonify({'input_error': {'message': 'Series is missing'}})
            # check if that series exists
            series = Series.query.filter_by(id=series_id).first()
            if not series:
                return jsonify({'input_error': {'message':'The series selected does not exist'}})
        else:
            series_id =None

        # create videos folder
        try:
            if not os.path.exists(app.config['VIDEOS_PATH_NAME']):
                original_umask = os.umask(0o000)
                os.makedirs(app.config['VIDEOS_PATH_NAME'], 0o777)
        except FileExistsError as e:
            pass
        except Exception as e:
            create_error_log(str(e))
            return jsonify({'db_error': {'message': 'Folder creation error'}})
        finally:
            os.umask(0o000)
        
        # create videos images folder        
        try:
            if not os.path.exists(app.config['VIDEOS_IMAGES_PATH_NAME']):
                original_umask = os.umask(0o000)
                os.makedirs(app.config['VIDEOS_IMAGES_PATH_NAME'], 0o777)
        except FileExistsError as e:
            pass
        except Exception as e:
            create_error_log(str(e))
            return jsonify({'db_error': {'message': 'Folder creation error'}})
        finally:
            os.umask(0o000)

        # generate slug for video
        slug = slugify(title)

        result = db.session.execute("SELECT * FROM videos where slug = '"+slug+"' AND id != '"+ str(this_video['id'])+"' LIMIT 1")
        i = 1
        while result.rowcount > 0:
            i = i + 1
            new_slug = slug + '_' + str(i)
            result = db.session.execute("SELECT * FROM videos where slug = '"+new_slug+"' AND id != '"+ str(this_video['id'])+"' LIMIT 1")
            if result.rowcount == 0:
                slug = new_slug
                break      
        
        # upload large image
        if large_image:
            ext = get_file_extension(large_image.filename)
            large_image_name = "large_" + public_id + "." + ext
            # remove old image
            if os.path.isfile(app.config['VIDEOS_IMAGES_PATH_NAME']+"/"+this_video['large_image']):
                os.remove(app.config['VIDEOS_IMAGES_PATH_NAME']+"/"+this_video['large_image'])
            large_image.save(os.path.join(app.config['VIDEOS_IMAGES_PATH_NAME'], large_image_name))
            video.large_image = large_image_name

        # upload portrait image
        if portrait_image:
            ext = get_file_extension(portrait_image.filename)
            portrait_image_name = "portrait_" + public_id + "." + ext
            # remove old image
            if os.path.isfile(app.config['VIDEOS_IMAGES_PATH_NAME']+"/"+this_video['portrait_image']):
                os.remove(app.config['VIDEOS_IMAGES_PATH_NAME']+"/"+this_video['portrait_image'])
            portrait_image.save(os.path.join(app.config['VIDEOS_IMAGES_PATH_NAME'], portrait_image_name))
            video.portrait_image = portrait_image_name

        # upload landscape image
        if landscape_image:
            ext = get_file_extension(landscape_image.filename)
            landscape_image_name = "landscape_" + public_id + "." + ext
            # remove old image
            if os.path.isfile(app.config['VIDEOS_IMAGES_PATH_NAME']+"/"+this_video['landscape_image']):
                os.remove(app.config['VIDEOS_IMAGES_PATH_NAME']+"/"+this_video['landscape_image'])
            landscape_image.save(os.path.join(app.config['VIDEOS_IMAGES_PATH_NAME'], landscape_image_name))
            video.landscape_image = landscape_image_name

        # upload square image
        if square_image:
            ext = get_file_extension(square_image.filename)
            square_image_name = "square_" + public_id + "." + ext
            # remove old image
            if os.path.isfile(app.config['VIDEOS_IMAGES_PATH_NAME']+"/"+this_video['square_image']):
                 os.remove(app.config['VIDEOS_IMAGES_PATH_NAME']+"/"+this_video['square_image'])
            square_image.save(os.path.join(app.config['VIDEOS_IMAGES_PATH_NAME'], square_image_name))
            video.square_image = square_image_name

        # upload video
        if video_file:
            # remove older video
            if os.path.isdir(app.config['VIDEOS_PATH_NAME']+"/"+public_id):
                shutil.rmtree(app.config['VIDEOS_PATH_NAME']+"/"+public_id)
            ext = get_file_extension(video_file.filename)
            video_file_name = "" + public_id + "." + ext
            video_file_type=ext
            video_file.save(os.path.join(app.config['VIDEOS_PATH_NAME'], video_file_name))
        
            # if user is admin, convert the saved file to mpd
            try:
                subprocess.run([app.config['PATH_NAME'] + "/transcode.py", app.config['VIDEOS_PATH_NAME']+"/"+video_file_name])
                os.remove(app.config['VIDEOS_PATH_NAME']+"/"+video_file_name)
                #video_folder_name = os.path.splitext(app.config['VIDEOS_PATH_NAME']+"/"+video_file_name)[0]
                video.video_file = video_file_name
                video.video_file_type = video_file_type
            except Exception as e:
                create_error_log(str(e))
                if os.path.isfile(app.config['VIDEOS_PATH_NAME']+"/"+video_file_name):
                    os.remove(app.config['VIDEOS_PATH_NAME']+"/"+video_file_name)
                return jsonify({'db_error': {'message':'Video conversion error'}})
            # save the video details.
            try:
                video.title = title
                video.slug = slug
                video.artist_id = artist_id
                video.type = video_type
                video.series_id = series_id
                video.description = description
                db.session.commit()

            except Exception as e:
                create_error_log(str(e))
                return jsonify({'db_error':{'message':'There was an internal error'}})

            # save the tag 
            if tags_arr:
                for tag_title in tags_arr:
                     # delete tags for this video from videos_tags_association table
                    db.engine.execute("DELETE FROM "+str(videos_tags_association)+" WHERE video_id = '"+str(this_video['id'])+"'")
                    video_tag = VideosTag.query.filter_by(title=tag_title).first()
                    if video_tag:
                        # get id and insert in into videos tag association table
                        tag_id = videos_tag_schema.dump(video_tag)['id']
                    else:
                        slug = slugify(tag_title)
                        video_tag_slug = VideosTag.query.filter_by(slug=slug).first()
                        i = 1
                        while video_tag_slug:
                            i = i + 1
                            new_slug = slug + '_' + str(i)
                            video_tag_slug = VideosTag.query.filter_by(slug=new_slug).first() 
                            if not video_tag_slug:
                                slug = new_slug
                                break
                        new_video_tag = VideosTag(tag_title, slug)
                        db.session.add(new_video_tag)
                        db.session.commit()
                        tag_id = new_video_tag.id
                    # insert into videos_tags_association table
                    db.engine.execute(videos_tags_association.insert(), tag_id=tag_id, video_id=this_video['id'])
            return jsonify({'response': {'message': 'Video updated successfully'}})
    except Exception as e:
        print(e)
        create_error_log(str(e))
        return jsonify({'db_error': {'message': 'There was an internal error'}})
#end edit video
# get single video
@app.route('/videos/get/<slug>', methods=['GET'])
@user_token_required
def get_single_video(current_user, slug):
    try:
        video = Video.query.filter_by(slug=slug).first()
        request = video_schema.dump(video)
        if video:
            return jsonify({'response': {'message':'Successful', 'result: ':request}})
        else:
            return jsonify({'not_found': {'message': 'Video not found'}})
    except Exception as e:
        create_error_log(str(e))
        return jsonify({'db_error': {'message':'There was an internal error'}})


@app.route('/videos/all', methods=['GET'])
@user_token_required
def get_all_videos(current_user):
    try:
        videos = Video.query.all()
        result = videos_schema.dump(videos)
        return jsonify({'response': {'message':'Successful', 'result': result}})
    except Exception as e:
        create_error_log(str(e))
        return jsonify({'db_error': {'message': 'There was a database error'}})


@app.route('/videos/accept/<public_id>', methods=['GET'])
@admin_token_required
def acccept_video_submission(current_admin, public_id):
    try:
        # get video details
        video = Video.query.filter_by(public_id=public_id).first()
        this_user = user_schema.dump(current_admin)
        # check if user has an artist account
        user_name = this_user['first_name']+" "+this_user['last_name']
        if not video:
            return jsonify({'response': {'message':'Video not found'}})
        this_video = video_schema.dump(video)
        # check video status
        if this_video['status'] != 'review':
            return jsonify({'response': {'message': 'This video is already accepted'}})
        else:
            # get the video file
            video_file_name = this_video['video_file']
            trailer = Trailer.query.filter_by(video_id=this_video['id']).first()  

            subprocess.run([app.config['PATH_NAME'] + "/transcode.py", app.config['VIDEOS_PATH_NAME']+"/"+video_file_name])
            os.remove(app.config['VIDEOS_PATH_NAME']+"/"+video_file_name)
            # save the video details.
            video.status = 'active'
            video.verified_by = user_name
            if trailer:
                this_trailer = trailer_schema.dump(trailer)
                if this_trailer['status'] == 'review':
                    trailer_file_name = this_trailer['trailer_file']
                    # convrt the trailer file
                    subprocess.run([app.config['PATH_NAME'] + "/transcode.py", app.config['TRAILERS_PATH_NAME']+"/"+trailer_file_name])
                    os.remove(app.config['TRAILERS_PATH_NAME']+"/"+trailer_file_name)
                    trailer.status = 'active'

            db.session.commit()
            return jsonify({'response': {'message': 'Video updated successfully'}})
    except Exception as e:
        create_error_log(str(e))
        return jsonify({'db_error': {'message': 'There was an internal error'}})
   
@app.route('/videos/activate/<public_id>', methods=['GET'])
@admin_token_required
def activate_a_video(current_admin, public_id):
    try:
        # get video details
        video = Video.query.filter_by(public_id=public_id).first()
        if not video:
            return jsonify({'response': {'message':'Video not found'}})
        this_video = video_schema.dump(video)
        # check video status
        if this_video['status'] != 'suspended':
            return jsonify({'response': {'message': 'This video is not suspended'}})
        else:
            # change the video details.
            try:
                video.status = 'active'
                db.session.commit()
                return jsonify({'response': {'message':'Video activited successfully'}})
            except Exception as e:
                create_error_log(str(e))
                return jsonify({'db_error':{'message':'There was an internal error'}})
    except Exception as e:
        create_error_log(str(e))
        return jsonify({'db_error': {'message': 'There was an internal error'}})

# trailer routes start
@app.route('/trailer', methods=['POST'])
@user_token_required
def add_trailer(current_user):
    try:
        uploaded_files=[]
        this_user = user_schema.dump(current_user)
        user_type = this_user['type']
        if user_type != 'artist' and user_type != 'admin': 
            return jsonify({'token_error': {'message': 'Kindly login'}})
        try:
            video_id = request.form['video_id']
            trailer_file = request.files['trailer_file']        
        except KeyError as e:
            return jsonify({'input_error': {'message': 'Some keys are missing'}})
        except Exception as e:
            create_error_log(str(e))
            return jsonify({'db_error': {'message': 'There was an internal error'}})
        
        public_id = str(uuid.uuid4())
        if not video_id:
            return jsonify({'input_error': {'message': 'Please select the video associated to this trailer'}})
        if not trailer_file:
            return jsonify({'input_error': {'message': 'Please add the trailer'}})
        video = Video.query.filter_by(id=video_id).first()
        if not video:
            return jsonify({'input_error': {'message': 'The video does not exist. Try again'}})
        #check if that video has a trailer
        existing_trailer = Trailer.query.filter_by(video_id = video_id).first()
        if existing_trailer:
            return jsonify({'input_error':{'message': 'That video already has a trailer. Go to videos on your dashboard, find the video and select edit'}})
        this_video = video_schema.dump(video)
        title = this_video['title']+' - Trailer'
        slug = slugify(title)
        status = 'review'

        trailer_slug = Trailer.query.filter_by(slug=slug).first()
        i = 1
        while trailer_slug:
            i = i + 1
            new_slug = slug + '_' + str(i)
            trailer_slug = Trailer.query.filter_by(slug=new_slug).first() 
            if not new_slug:
                slug = new_slug
                break
        # create trailes folder        
        try:
            if not os.path.exists(app.config['TRAILER_PATH_NAME']):
                original_umask = os.umask(0o000)
                os.makedirs(app.config['TRAILER_PATH_NAME'], 0o777)
        except FileExistsError as e:
            pass
        except Exception as e:
            create_error_log(str(e))
            return jsonify({'db_error': {'message': 'Folder creation error'}})
        finally:
            os.umask(0o000)
        # upload trailer file
        ext = get_file_extension(trailer_file.filename)
        trailer_file_name = "" + public_id + "." + ext
        trailer_file_type=ext
        trailer_file.save(os.path.join(app.config['TRAILER_PATH_NAME'], trailer_file_name))


        # if user is admin, convert the saved file to mpd
        if user_type == 'admin':
            status = 'active'
            try:
                subprocess.run([app.config['PATH_NAME'] + "/transcode.py", app.config['TRAILER_PATH_NAME']+"/"+trailer_file_name])
                os.remove(app.config['TRAILER_PATH_NAME']+"/"+trailer_file_name)
                trailer_folder_name = os.path.splitext(app.config['VIDEOS_PATH_NAME']+"/"+trailer_file_name)[0]
                
                uploaded_files.append(trailer_folder_name)
            except Exception as e:
                create_error_log(str(e))
                delete_files(uploaded_files)
                return jsonify({'db_error': {'message':'Trailer Conversion error'}})

        new_trailer = Trailer(public_id, video_id, title, slug, status, trailer_file_name, trailer_file_type)
        db.session.add(new_trailer)
        db.session.commit()
        #update the videos table
        video.trailer_id = new_trailer.id
        db.session.commit()

        return jsonify({'response': {'message': 'The trailer was added successfully'}})
    except Exception as e:
        create_error_log(str(e))
        delete_files(uploaded_files)
        return jsonify({'db_error': {'message': 'There was a database error'}})
# trailer routes end

class UserSchema(ma.Schema):
    class Meta:
        fields = ('id', 'public_id', 'first_name', 'last_name', 'email', 'phone', 'profilePhoto', 'type')

class VideoSchema(ma.Schema):
    class Meta:
        strict = True
        fields = ('id','public_id', 'title', 'slug', 'status', 'artist_id', 'duration', 'type', 'series_id', 'trailer_id', 'video_file', 'large_image', 'portrait_image','landscape_image', 'square_image', 'description', 'created_on')

class TrailerSchema(ma.Schema):
    class Meta:
        fields = ('id','public_id', 'video_id', 'status', 'title', 'slug', 'trailer_file', 'created_on') 

user_schema = UserSchema()
users_schema = UserSchema(many=True)

artist_schema = ArtistSchema()
artists_schema = ArtistSchema(many=True)


video_schema = VideoSchema()
videos_schema = VideoSchema(many=True)


videos_tag_schema = VideosTagSchema()
videos_tags_schema = VideosTagSchema(many=True)

trailer_schema = TrailerSchema()
trailers_schema = TrailerSchema(many=True)


@app.route('/login')
@cross_origin(supports_credentials=True)
def login():
    try:
        auth = request.authorization

        if not auth or not auth.username or not auth.password:
            #return make_response('Could not verify it', 401, {'WWW-Authenticate': 'Basic realm = "Login required!"'})
            return jsonify({'auth_error': {'message': 'Kindly login'}})
        
        user = User.query.filter_by(email=auth.username).first()
        if not user:
            # return make_response('Wrong credentails', 401, {'WWW-Authenticate': 'Basic realm = "Login required!"'})
            return jsonify({'auth_error': {'message': 'Wrong credentials'}})

        if bcrypt.check_password_hash(user.password, auth.password):
            token = jwt.encode({'public_id': user.public_id, 'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=120)}, app.config['SECRET_KEY'])

            return jsonify({'response': {'message': 'successful', 'token': token.decode('UTF-8')}})
        
        # return make_response('Failed to verify', 401, {'WWW-Authenticate': 'Basic realm = "Login required!"'})
        return jsonify({'auth_error': {'message': 'Kindly login'}})
    except Exception as e:
        create_error_log(str(e))
        return jsonify({"db_error": {'message': 'There was a database error'}})




# Run server
if __name__ == '__main__':
    app.run(debug=True)